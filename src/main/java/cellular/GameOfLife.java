package cellular;

import java.util.Objects;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		int width = this.numberOfColumns();
		int height = this.numberOfRows();

		for(int v = 0; v < height; v++){
            for(int h = 0; h < width; h++){
				
				nextGeneration.set(v,h,this.getNextCell(v,h));
			}
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		
		CellState self = this.getCellState(row, col);
        CellState next = getCellState(row, col);
		int count = countNeighbors(row, col, CellState.ALIVE);

		if(Objects.equals(self, CellState.ALIVE)){ //alive
			
			if(count <= 1){
				next = CellState.DEAD;
			}
			else if(count >= 4){
				next = CellState.DEAD;
			}
			else if(count <= 3 && count >=2)
			{
				next = CellState.ALIVE;
			}
		}
		else //dead
		{
			if(count == 3){
				next = CellState.ALIVE;
			}
		}

		return next;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {

		int sum = 0;


		for(int y = row-1; y <= row+1; y++){
			for(int x = col-1; x <= col+1; x++){
				if(	(y < 0) || (y >= this.numberOfRows()))
				{
					continue;
				}

				if(	(x < 0) || (x >= this.numberOfColumns()))
				{
					continue;
				}
					
				if (y == row && x == col){
					continue;
				}

					if(Objects.equals(state, this.getCellState(y, x))){
						sum ++;
					}
			}
		}
		return sum;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
