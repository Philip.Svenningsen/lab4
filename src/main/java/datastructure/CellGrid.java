package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int row_count;
    private int col_count;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		row_count = rows;
        col_count = columns;
        grid = new CellState[row_count][col_count];
        for(int v = 0; v < row_count; v++){
            for(int h = 0; h < col_count; h++){
                grid[v][h] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
       return row_count;
    }

    @Override
    public int numColumns() {
       return col_count;
    }

    @Override
    public void set(int row, int column, CellState element) {

        if(row >= row_count || row < 0 || column >= col_count || column < 0)
        {
            throw new IndexOutOfBoundsException("Not a real coordiate within the grid");
        }
        else
        {
            grid[row][column] = element;
        }
        
    }

    @Override
    public CellState get(int row, int column) {
        
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid LocalGrid = new CellGrid(row_count, col_count,null);
        for(int v = 0; v < row_count; v++){
            for(int h = 0; h < col_count; h++){
                LocalGrid.set(v,h,this.get(v,h));
            }
        }
        return LocalGrid;
    }
    
}
